from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
     first_name = forms.CharField(max_length=30, required=True, help_text='Obligatoire.')
     last_name = forms.CharField(max_length=30, required=True, help_text='Obligatoire.')
     email = forms.EmailField(max_length=254, help_text='Veuillez entrer un email valide.')

     class Meta:
         model = User
         fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )
     first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')

class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
