# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from forms import *
from django.contrib.auth import logout
from django.shortcuts import redirect

from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@csrf_exempt
def connexion (request):
    error = False

    if request.method == "POST":
        form = ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            print ("User =" + username + "Passwd " + password)

            if user:
                login(request, user)
                return redirect('maz/listeModeles')
            else:
                error = True
    else:
        form = ConnexionForm()

    return redirect('accueil')

def register(request):

    error = False

    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            #form.save()
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]
            nom = form.cleaned_data["last_name"]
            prenom = form.cleaned_data["first_name"]
            email = form.cleaned_data["email"]
            #Creer l'utilisateur
            user = User.objects.create_user(username, email, password)
            user.first_name, user.last_name = prenom, nom
            user.save();
            return redirect('maz/accueil')

    else :
        form = RegisterForm()

    return render(request, 'register.html', locals())

def deconnexion(request):
    logout(request)
    return redirect('accueil')
