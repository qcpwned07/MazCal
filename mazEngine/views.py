# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse

#Create your views here.
def home(request): 
    return render(request, 'accueil.html', locals())

def accueil(request):
    if request.user.is_authenticated :
        return redirect('maz/listeModeles')
    else:
        return render(request, 'accueil.html', locals())

def close(request):
    return render(request, 'close.html', locals())
