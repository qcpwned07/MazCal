from django import forms
from django.contrib.auth.models import User
from .models import * #Contexte, Modele, Donnee, Variable, Entree, Parametre

class CreateModeleForm(forms.Form):
    nVar = 5
    nom = forms.CharField(label="Nom",max_length=100)
    description = forms.CharField(widget=forms.Textarea, label="Description")
    createur = ""

    #Pour avoir le createur
    def __init__(self, user, *args, **kwargs):
        self.createur = user
        super(CreateModeleForm, self).__init__(*args, **kwargs)

    def save(self):
        c = Modele()
        c.nom  = self.cleaned_data['nom']
        c.description = self.cleaned_data['description']
        c.createur = User.objects.get(pk=self.createur.id)
        c.save()

class CreateContexteForm(forms.Form):
    #Forme rapide pour creer un formulaire
    #class Meta:
    #    model = Contexte
    #    fields = '__all__'
    nom = forms.CharField(label="Nom",max_length=100)
    description = forms.CharField(widget=forms.Textarea, label="Description")

    
    def __init__(self, parent, *args, **kwargs):
        self.parent = Modele.objects.get(id=parent)
        super(CreateContexteForm, self).__init__(*args, **kwargs)

    def save(self):
        c = Contexte()
        c.nom  = self.cleaned_data['nom']
        c.description = self.cleaned_data['description']
        c.modeleParent = self.parent
        c.save()

    #var = forms.CharField(label="Parametre 1",max_length=100)

class InsertDonneeForm(forms.Form):
    variable = Variable()
    nomVar = 'Variable'
    valeur = forms.CharField(label=nomVar, max_length=100)
    
    def setList(self, variable):
        choices = ListItem.objects.filter(variable=variable, modele=variable.modele)
        valeur = forms.ModelChoiceField(queryset=choices)# or None)
        return valeur


class FamilleForm(forms.ModelForm):
    #forme rapide pour creer un formulaire
    class Meta:
        model = Parametre 
        fields = ['nom']

class VariableForm(forms.ModelForm):
    #forme rapide pour creer un formulaire
    class Meta:
        model = Variable 
        fields = ['nom', 'parametre', 'type_variable', 'borneMin', 'borneMax']

class EntreeForm(forms.ModelForm):
    #forme rapide pour creer un formulaire
    class Meta:
        model = Entree 
        fields = ['contexte']

class ListItemForm(forms.ModelForm):
    class Meta:
        model = ListItem 
        fields = ['value', 'intValue']
