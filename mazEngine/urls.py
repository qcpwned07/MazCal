
from django.views.generic import TemplateView
from django.conf.urls import url
from django.contrib import admin
from .views import home, accueil, close
from django.conf.urls import include
from . import crud
from users import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #Inclure la vue home (dans le fichier blog)
    url(r'^accueil/?$', home),
    url(r'^accueil', accueil, name="accueil"),
    url(r'^close', close, name="close"),
    # Create
    url(r'^creerContexte/(?P<parent>\d+)', crud.createContexteForm, name="createContexteForm"),
    url(r'^createVariables/(?P<parent>\d+)', crud.createVariables, name="createVariables"),
    url(r'^createFamily/(?P<parent>\d+)', crud.createFamily, name="createFamily"),
    url(r'^createVariables/(?P<parent>\d+)/(?P<n>\d+)', crud.createVariables, name="createVariables"),
    url(r'^addListItem/(?P<parent>\d+)', crud.addListItem, name="addListItem"),
    url(r'^creerModele', crud.createModeleForm, name="createModeleForm"),
    url(r'^insererDonnee/(?P<modele>\d+)', crud.insererDonnee, name="insererDonnee"),
    # Afficher
    url(r'^afficherModele/(\d+)$', crud.afficherModele, name="afficherModele"),
    url(r'^afficherDonnee/(\d+)$', crud.afficherModele, name="afficherDonnee"),
    url(r'^compare$', crud.compareView, name="compare"),
    # Edit
    url(r'^edit/(?P<pk>\d+)$', crud.ModeleUpdate.as_view(), name='modele_edit'),
    url(r'^editModele/(?P<pk>\d+)$', crud.ModeleUpdate.as_view(), name='modele_edit'),
    url(r'^editContexte/(?P<pk>\d+)$',crud.ContexteUpdate.as_view(),name='contexte_edit'),
    url(r'^editVar/(?P<pk>\d+)$', crud.VariableUpdate.as_view(), name='variable_edit'),
    url(r'^editFamily/(?P<pk>\d+)$', crud.FamilyUpdate.as_view(), name='family_edit'),

    url(r'^listeModeles', crud.ModeleList.as_view(), name="modele_list"),
    url(r'^listeModeles', crud.ModeleList.as_view(), name="listeModeles"),
    url(r'^userModeles', crud.UserModeles.as_view(), name="userModeles"),
      #url(r'^$', views.ModeleList.as_view(), name='modele_list'),
    url(r'^new$', crud.CreateModeleForm, name='modele_new'),
    url(r'^delete/(?P<pk>\d+)$', crud.deleteModel, name='modele_delete'),
    url(r'^deleteEntry/(?P<entry>\d+)$', crud.deleteEntry, name='entry_delete'),
    url(r'^supprimerModele/(?P<pk>\d+)', crud.deleteModel, name="modele_delete"),
    # USER management
    url(r'^logout$', views.deconnexion, name ='deconnexion'),
    url(r'^register$', views.register, name ='register'),
    url(r'^request/(?P<modele>\d+)$', crud.contributorRequest, name ='modele_request'),
    url(r'^requestaccept/(?P<modele>\d+)/(?P<user>\d+)$', crud.acceptContributor, name ='accept_contributor'),
    url(r'^requestremove/(?P<modele>\d+)/(?P<user>\d+)$', crud.deleteContributor, name ='delete_contributor'),
    # API
    url(r'^api-instructions$',TemplateView.as_view(template_name="api.html"), name='api'),
    url(r'^compare/(?P<c1>\d+)/(?P<c2>\d+)', crud.compare, name="compare2"),
    url(r'^compare/(?P<c1>\d+)/(?P<c2>\d+)', crud.compare, name="compare2"),
    url(r'^models$', crud.modelsAPI, name ='models'),
    url(r'^models/(\d+)$', crud.modelsAPI, name ='models'),
    ]

