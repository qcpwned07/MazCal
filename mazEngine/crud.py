# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.forms.models import model_to_dict
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from models import *
from forms import *
#Pour le formset
from django.forms import formset_factory

def close(request):
    return render(request, 'close.html', locals())
# ---------------- #
# -----MODELE----- #
# ---------------- #

def createModeleForm(request):
    FamilleFormSet = formset_factory(FamilleForm, extra=6)
    if request.POST:
        form = CreateModeleForm(request.user, request.POST, prefix="form")
        formset = FamilleFormSet(request.POST, prefix="formset")
    else:
        form = CreateModeleForm(request.user)
        formset = FamilleFormSet(prefix="formset")


    #Creer les variables locales utilisee dans la vue
    submitAdress = "createModeleForm"
    form_type = "Model"
    form_title = " Create a new Model"
    formset_title = "Parameters families"
    formset_type = "Family"
    actionMenu = True
    actions = {}

    #Methode qui retourne vrai s'il y a des erreurs
    if form.is_valid() and formset.is_valid():
        #Ici nous traitons les donnes du formulaire
        nom = form.cleaned_data['nom']
        description = form.cleaned_data['description']
        form.save()
        parent = Modele.objects.get(nom=nom).id
        # Nous pourrions ici envoyer l'e-mail grâce aux données 
        # que nous venons de récupérer
        envoi = True
        #Traiter les donnees du formSet
        for varform in formset:
            #nom = form.cleaned_data['nom']
            if(varform.cleaned_data):
                param = Parametre()
                param.modele = Modele.objects.get(pk=parent)
                param.nom  = varform.cleaned_data['nom']
                param.save()

        return redirect('createVariables', parent  )
        
    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'createForm.html', locals())


def afficherModele(request, id):
    try:
        modele = Modele.objects.get(pk=id)   #Get le modele
        variables = Variable.objects.filter(modele=id)
        parametres = Parametre.objects.filter(modele=id)
        contextes = Contexte.objects.filter(modeleParent=id)
        entries = Entree.objects.filter(modele=id)
        listItem = ListItem.objects.filter(variable__in=list(variables))
        contributors = listContributors(modele)
        contributorRequests = listContributorRequest(modele)
    except Modele.DoesNotExist:
        raise Http404
    except ValueError:
        "Do nothing"
    #Action menu
    canEdit = canUpdate(modele, request.user)
    actionMenu = True
    actions = {}
    actions['Add a context'] = ["createContexteForm", id]
    actions['Make a data entry'] = ["insererDonnee", id]

    return render(request, 'afficherModele.html', locals())

def contributorRequest(request, modele):
    try:
        auth = Authorization.objects.get(user=request.user, modele=modele)
    except:
        auth = Authorization(user=User.objects.get(id=request.user.id), status=0)
        auth.modele = Modele.objects.get(pk=modele)
        auth.save()
    return redirect('afficherModele', modele)

def listContributorRequest(modele):
    return list(Authorization.objects.filter(modele=modele, status=0))

def listContributors(modele):
    return list(Authorization.objects.filter(modele=modele, status=1))

def acceptContributor(request, modele, user):
    auth = Authorization.objects.get(
            user=user,
            modele=modele)
    auth.status = 1
    auth.save()
    return redirect('afficherModele', modele)

def deleteContributor(request, modele, user):
    Authorization.objects.get(user=user, modele=modele).delete()
    return redirect('afficherModele', modele)

# ---------------- #
# ----COMPARE----- #
# ---------------- #
def compareView(request):
    try:
        contextes = request.POST.getlist('contexte')
        contextes = [int(c) for c in contextes]
        contexte1 = Contexte.objects.get(pk=contextes[0])
        if (len(contextes) > 1):
            contexte2 = Contexte.objects.get(pk=contextes[1])

        #modele = Modele.objects.get(pk=id)   #Get le modele
        contextes = Contexte.objects.all().filter(pk__in = contextes)
        modele = contextes[0].modeleParent
        lignes = getLignes(modele)                      #Get les lignes du modele
        parametres = Parametre.objects.filter(modele=modele.id)
        print (lignes)
        contextes = Contexte.objects.filter(modeleParent=modele.id)
    except Modele.DoesNotExist:
        raise Http404
    #Action menu
    id = modele.id
    actionMenu = True
    actions = {}    
    actions['Ajouter un contexte'] = ["createContexteForm", id]
    actions['Insérer des données'] = ["insererDonnee", id]

    

    return render(request, 'compare.html', locals())

# ---------------- #
# --Modele CRUD--- #
# ---------------- #
class UserModeles(ListView):
    model = Modele 
    def get_queryset(self):
        modeles = super(UserModeles, self).get_queryset()
        return modeles.filter(createur=self.request.user)
    def get_context_data(self, **kwargs):
        ctx = super(ModeleList, self).get_context_data(**kwargs)
        ctx['actionMenu'] = True
        return ctx


class ModeleList(ListView):
    model = Modele

    #Pour passer des variables 
    def get_context_data(self, **kwargs):
        ctx = super(ModeleList, self).get_context_data(**kwargs)
        ctx['actionMenu'] = True
        return ctx

class ModeleUpdate(UpdateView):
    #Pour l'updateView normal
    model = Modele
    fields = ['nom', 'description']
    actions = {}
    #Mes additions
    def get_context_data(self, **kwargs):
        self.actions['Ajouter un contexte'] = ["createContexteForm", self.kwargs['pk']]
        self.actions['Insérer des données'] = ["insererDonnee", self.kwargs['pk']]
        ctx = super(ModeleUpdate, self).get_context_data(**kwargs)
        ctx['params'] = True
        ctx['actionMenu'] = True
        ctx['title'] = Modele.objects.get(pk=self.kwargs['pk'])
        ctx['contextes']= Contexte.objects.filter(modeleParent=self.kwargs['pk'])
        ctx['actions'] = self.actions
        return ctx
    


def  deleteModel(request, pk):
    Modele.objects.get(pk=pk).delete()
    return redirect('modele_list')
# ---------------- #
# ----CONTEXTE---- #
# ---------------- #

class ContexteUpdate(UpdateView):
    #Pour l'updateView normal
    model = Contexte
    fields = ['nom', 'description']
    actions = {}

def createContexteForm(request, parent):
    #Construire le formulaire
    #form = CreateContexteForm(request.POST or None)
    if request.method == 'POST':
        form = CreateContexteForm(parent, request.POST,prefix="form")
            
    else:
        form = CreateContexteForm(parent, prefix="form")
    #if request.POST:
    #    form = CreateContexteForm(parent, request.POST)
    #else:
    #    form = CreateContexteForm(parent)

    #Creer les variables locales utilisee dans la vue
    submitAdress = "createContexteForm"
    submitId = parent
    form_type = "Contexte"
    form_title = "Create a new context"

    #Verifier si tout est en regle
    if form.is_valid():
        #Form
        nom = form.cleaned_data['nom']
        description = form.cleaned_data['description']
        contexte = form.save() 
        return redirect('afficherModele', parent)

    return render(request, 'createForm.html', locals())

# ---------------- #
# ----VARIABLES--- #
# ---------------- #

def createVariables (request, parent, n=1):
    VariablesFormSet = formset_factory(VariableForm, extra=n) 
    formset = VariablesFormSet(request.POST or None)
    submitAdress = "createVariables"
    submitId = parent
    form_title = " Create variables"
    formset_type = "Variable"
    

    if formset.is_valid():
        for form in formset:
            if( form.cleaned_data):
                variable = Variable()
                variable.modele = Modele.objects.get(pk=parent)
                variable.nom = form.cleaned_data['nom']
                variable.borneMin = form.cleaned_data['borneMin']
                variable.borneMax = form.cleaned_data['borneMax']
                variable.type_variable = form.cleaned_data['type_variable']
                variable.save()
                variable.parametre = form.cleaned_data['parametre']
                variable.save() 
        return redirect('close')

    return render(request, 'popupForm.html', locals())

class VariableUpdate(UpdateView):
    #Pour l'updateView normal
    model = Variable
    fields = ['nom']
    actions = {}

def addListItem(request,parent):
    form = ListItemForm(request.POST or None)
    submitAdress = "addListItem"
    submitId = parent
    form_title="Add a choice to the list"

    if form.is_valid():
        li = ListItem()
        li.variable = Variable.objects.get(pk=parent)
        li.modele = li.variable.modele
        li.intValue = form.cleaned_data['intValue']
        li.value = form.cleaned_data['value']
        li.save()
        return redirect('close')
    return render(request, 'popupForm.html', locals())

class ListItemUpdate(UpdateView):
    #Pour l'updateView normal
    model = ListItem
    fields = ['value', 'intValue']

def createFamily (request, parent, n=1):
    form = FamilleForm(request.POST or None)
    submitAdress = "createFamily"
    submitId = parent
    form_title= "Create variable family"

    if form.is_valid():
        fam = Parametre()
        fam.nom = form.cleaned_data['nom']
        fam.modele = Modele.objects.get(pk=parent)
        fam.save()
        return redirect('close')
    return render(request, 'popupForm.html', locals())

class FamilyUpdate(UpdateView):
    #Pour l'updateView normal
    model = Parametre
    fields = ['nom']

# ---------------- #
# -----DONNEE----- #
# ---------------- #

def insererDonnee(request, modele, contexte=-1):
    form = EntreeForm(request.POST or None)
    variables = Variable.objects.filter(modele=modele)
    varIter = variables.iterator()
    DonneeFormSet = formset_factory(InsertDonneeForm, extra=variables.count()) 
    submitAdress = "insererDonnee"  
    submitId = modele
    form_type = "Data entry"
    form_title = "Insert new data in context :"
    formset_type = "Variable"
    # 
    if request.POST:
        formset = DonneeFormSet(request.POST)
    else:
        formset = DonneeFormSet()
    for formx in formset:
        formx.variable  = varIter.next()
        formx.nomVar = formx.variable.nom
        formx.fields['valeur'].label = formx.nomVar
        print formx.variable.type_variable
        if formx.variable.type_variable in ["SOLIST", "LIST"]:
            formx.setList(formx.variable)
            print formx.nomVar
            formx.fields['valeur']= formx.setList(formx.variable)
    # Verifier si tout est en regle
    if form.is_valid():
        #Ici nous traitons les donnes du formulaire
        entree = Entree()
        entree.contexte = form.cleaned_data['contexte'] if contexte==-1 else contexte
        entree.auteur = request.user.username
        entree.modele = Modele.objects.get(pk=modele)
        entree.save()
        for formx in formset:
            if formx.is_valid():
                donnee = Donnee()
                donnee.variable = formx.variable
                donnee.entree = entree #Entre.objects.get(pk=entree.id)
                donnee.modele = entree.modele
                donnee.contexte = entree.contexte
                donnee.valeur = formx.cleaned_data['valeur']
                # Si liste, aller chercher la valeur
                if donnee.variable.type_variable in ["LIST", "SOLIST"]:
                    print donnee.valeur
                    print ListItem.objects.get(value=donnee.valeur).intValue
                    donnee.intValeur = ListItem.objects.get(value=donnee.valeur).intValue
                else:
                    donnee.intValeur = int(formx.cleaned_data['valeur'])
                donnee.save()
        return redirect('afficherModele', modele)
        #contexte = form.cleaned_data['contexte']
        #var1 = form.cleaned_data['var']
    
        envoi = True
    return render(request, 'createForm.html', locals())

def deleteEntry(request, entry):
    model = Entree.objects.get(pk=entry).modele
    Entree.objects.get(pk=entry).delete()
    return redirect('afficherModele', model.id)

# -------------------- #
# ----FONCTIONS/API--- #
# -------------------- #
def compareAPI (request, c1, c2, variables=[]):
    return HttpResponse(json.dump(compare(c1,c2,variables)))

def compare (c1, c2, variables=[]):
    data1=[]
    data2=[]
    lines = []
    # Validation
    if c1.isdigit() and c2.isdigit():
        c1 = Contexte.objects.get(pk=c1)
        c2 = Contexte.objects.get(pk=c2)
    if c1.modeleParent != c2.modeleParent:
        return HttpResponseBadRequest
    # Getting info
    if not variables:
        variables = Variable.objects.filter(modele=c1.modeleParent)
    else : variables = Variable.objects.filter(pk__in = variables)
    for v in variables:
        try:
            data1.append(Donnee.objects.get(contexte=c1, variable=v))
        except:
            data1.append(None)
        try:
            data2.append(Donnee.objects.get(contexte=c2, variable=v))
        except:
            data2.append(None)
    # Computing gap and saving
    for d1, d2 in zip(data1, data2):
        if d1 is not None:
            if d2 is not None:
                lines.append([d1.variable.id, d1.variable.nom, d1.valeur,d2.valeur, 
                    getEcart(d1, d2)])
            else:
                lines.append([d1.variable.id, d1.variable.nom, d1.valeur, None, 
                    getEcart(d1, d2)])
        elif d2 is not None:
            if d1 is not None:
                lines.append([d1.variable.id, d2.variable.nom,d1.valeur, d2.valeur, 
                    getEcart(d1, d2)])
            else:
                lines.append([d1.variable.id, d2.variable.nom, None, d2.valeur,
                    getEcart(d1, d2)])
    return lines

def modelsAPI(request, id=None):
    myModel = {}

    if id is not None:
        if id.isdigit():
            model = Modele.objects.get(pk=id)
        else:
            model = Modele.objects.get(nom=id)
        myModel['id'] = model.id
        myModel['name'] = model.nom
        myModel['description'] = model.description
        myModel['contexts'] = list(map(
                lambda c: (c.id, c.nom, c.description),
                list(Contexte.objects.filter(modeleParent=model)) ))
        #for c in myModels.contexts: c = (c.id, c.nom, c.description)
        myModel['parameters'] = map(lambda p: p.nom, 
                list(Parametre.objects.filter(modele=model)) )
        myModel['variables'] = map( lambda v: model_to_dict(v),
                list(Variable.objects.filter(modele=model)) )
        for v in myModel['variables']:
            v['parametre'] = map(lambda p: p.nom,  list(v['parametre']) )
    else :
        myModels = list(Modele.objects.all()) 
        myModel = list( map(lambda m: (m.id, m.nom, m.description), myModels) )


    return HttpResponse(json.dumps(myModel))
    

def getLignes(modele):
    lignes = []
    data =[]
    variables = list(Variable.objects.filter(modele=modele)) or []
    contextes = Contexte.objects.filter(modeleParent=modele) or []
    parametres = Parametre.objects.filter(modele=modele) or []

    for v in variables :                    #Prendre les donnee (data)
        for c in contextes:
            try:
                data.append( Donnee.objects.get(variable=v,contexte=c) )
            except Donnee.DoesNotExist:
                x = "Do nothing"
    # Ajouter le code pour les calculs d'ecart
    
    if variables!=None:
        for v in variables :                    #Mettre sous forme de tableau 2d 
            ecart = 100
            dataLigne =[]
            valeurs =[]
            dataLigne.append(v.nom)
            
            for d in data:                      #Inserer les points de donnee
                if (d.variable == v) and (d.contexte in contextes):
                    dataLigne.append(d.valeur)
                    valeurs.append(d)

            if 1 < len(valeurs):
                ecart = getEcart(valeurs[0], valeurs[1])
            elif 0 < len(valeurs):
                ecart = getEcart(valeurs[0])
            else:
                ecart = 0

            dataLigne.append(ecart)
            lignes.append(dataLigne)
        
    return lignes

def getEcart2(min,max, val):
    if len(val)==1:
        return 0
    print  val
    max = max - min
    val[0] -= min
    val[1] -= min
    val[0] /= max
    print val
    val[1] /= max
    return abs(val[1]-val[0])

def getEcart(data, data2=None):
    ecart = 1
    if (data is not None) and (data2 is not None):
        min = float(data.variable.borneMin)
        max = float(data.variable.borneMax - min)
        ecart = abs( ((data.intValeur-min) / max) - ((data2.intValeur-min) / max) )
    return ecart
  
def canUpdate(modele, user):
    try:
        Authorization.objects.get(modele=modele, user=user, status=1)
        canUpdate = True
    except:
        canUpdate = False
    if (not canUpdate):
        if user == modele.createur:
            canUpdate = True
    return canUpdate
