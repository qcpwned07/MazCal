# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import * #Contexte, Modele, Donnee, Parametre, Variable, Entree

#{ Register your models here
admin.site.register(Contexte)
admin.site.register(Modele)
admin.site.register(Donnee)
admin.site.register(Parametre)
admin.site.register(Variable)
admin.site.register(Entree)
admin.site.register(ListItem)
admin.site.register(Authorization)
