# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Modele(models.Model):
    nom         = models.CharField(max_length=100, unique=True)
    description = models.TextField(null=True)
    createur    = models.ForeignKey(User)

    def __unicode__(self):
        return self.nom
    def get_absolute_url(self):
        return reverse('modele_edit', kwargs={'pk': self.pk})
    def __str__(self):
        """ 
        Methode definie dans tout les modeles permettant
        de reconnaitre facilement les differents objets 
        qui seront traite par la suite.
        """
        return self.nom

class Contexte(models.Model):
    nom         = models.CharField(max_length=100,default="Contexte")
    description = models.TextField(null=True)
    modeleParent= models.ForeignKey(Modele)

    def __str__(self):
        """
        Methode pour reconnaitre les modeles, a mettre partout
        """
        return self.nom
    def get_absolute_url(self):
        return reverse('contexte_edit', kwargs={'pk': self.pk})


class Parametre (models.Model):
    nom         = models.CharField(max_length=200)
    modele      = models.ForeignKey(Modele)

    def get_absolute_url(self):
        return reverse('family_edit', kwargs={'pk': self.pk})
    def __str__(self):
        """
        Methode pour reconnaitre les modeles, a mettre partout
        """
        return self.nom

class Variable (models.Model):
    CHOIX_TYPE = (
            ('INT' , 'Integer'),
            ('CHAR', 'Character'),
            ('FLOAT', 'Floating point number'),
            ('STRING', 'String'),
            ('BOOL', 'Boolean'),
            ('LIST', 'List'),
            ('SOLIST', 'Soft list'),
    )

    nom         = models.CharField(max_length=200)
    parametre   = models.ManyToManyField(Parametre)
    modele      = models.ForeignKey(Modele)
    borneMin    = models.IntegerField(null=True)
    borneMax    = models.IntegerField(null=True)

    type_variable = models.CharField(
            max_length=6,
            choices=CHOIX_TYPE,
            default='INT',
    )

    def get_absolute_url(self):
        return reverse('variable_edit', kwargs={'pk': self.pk})
    def __str__(self):
        """
        Methode pour reconnaitre les modeles, a mettre partout
        """
        return self.nom

class Entree (models.Model):
    auteur      = models.CharField(max_length=100, default='User')
    modele      = models.ForeignKey(Modele)
    contexte    = models.OneToOneField(
                    Contexte,
                    on_delete=models.CASCADE,
                    primary_key=True,
                )
    date        = models.DateTimeField(
                    auto_now_add=True, #auto_now=True, 
                    verbose_name="Date d'entree", 
                    null=True
                  )

    def __str__(self):
        """
        Methode pour reconnaitre les modeles, a mettre partout
        """
        return self.auteur +" " + self.contexte.nom

class Donnee (models.Model):
    variable    = models.ForeignKey(Variable)
    valeur      = models.CharField(max_length=100)
    intValeur   = models.IntegerField(max_length=100)
    entree      = models.ForeignKey(Entree)
    contexte    = models.ForeignKey(Contexte)

    def __str__(self):
        " Methode pour reconnaitre les modeles, a mettre partout"
        return self.variable.nom +': ' + self.valeur 

class ListItem (models.Model):
    modele = models.ForeignKey(Modele)
    variable = models.ForeignKey(Variable)
    value = models.CharField(max_length=100)
    intValue = models.PositiveIntegerField(
            default=1,
            validators=[MaxValueValidator(100), MinValueValidator(1)])

    def get_absolute_url(self):
        return reverse('listItem_edit', kwargs={'pk': self.pk})
    def __str__(self):
        " Methode pour reconnaitre les modeles, a mettre partout"
        return self.value

class Authorization (models.Model):
    modele = models.ForeignKey(Modele)
    user = models.ForeignKey(User)
    status = models.IntegerField()
    def __str__(self):
        " Methode pour reconnaitre les modeles, a mettre partout"
        return self.model + " " + self.user


